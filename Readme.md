# Tracking Sheet - Polymer Project

- [Tracking Sheet - Polymer Project](#tracking-sheet---polymer-project)
  - [09/01: Project start](#0901-project-start)
  - [16/01: Technology discussion with teachers](#1601-technology-discussion-with-teachers)
  - [19/01: Meeting with the customers](#1901-meeting-with-the-customers)
  - [23/01: Choice of technologies](#2301-choice-of-technologies)
  - [30/01: Training - Basic project preparation](#3001-training---basic-project-preparation)
  - [06/02: Training - Beginning of tooling](#0602-training---beginning-of-tooling)
  - [13/02: Pedagogical break](#1302-pedagogical-break)
  - [20/02: Review of the code base - Preparation of the workflow](#2002-review-of-the-code-base---preparation-of-the-workflow)
  - [27-28/02: Slide preparation - Additional training - Weekly task list](#27-2802-slide-preparation---additional-training---weekly-task-list)
  - [06-07/03 : Slider Component(front) - Login Page(back ~, front) - Begining of all view basicly(front)](#06-0703--slider-componentfront---login-pageback--front---begining-of-all-view-basiclyfront)
  - [13-14/03 : Begining of experiment (back) - AdminPanel (back) - All view (front)](#13-1403--begining-of-experiment-back---adminpanel-back---all-view-front)
  - [20-21/03 :  - Track progress of users (back/front) - Help Button (front)](#20-2103-----track-progress-of-users-backfront---help-button-front)
  - [27-28/03 : rien](#27-2803--rien)


## 09/01: Project start
- Project selection

## 16/01: Technology discussion with teachers
- Discovery of the project, preparation of the contact with the person in charge of the material field in order to better understand the subject, beginning of reflexions around the technology to use.

## 19/01: Meeting with the customers

- The project group met with the material students responsible for the experiments of the escape game. They explained what they hoped to have as a website with their requirements and optional elements. It was discussed what was feasible within the time frame that is allocated for the project. The groups agreed on how to communicate with each other.  

## 23/01: Choice of technologies
- Fixed technologies : 
    - front : vuejs3 (js framework for SPA creation), vitest (test), typescript (typing)
    - back : nodejs (js server), jest (test), typescript(typing), swagger(documentation), sequelize (orm), postgresql (database)
    - tools : docker (container), gitlab ci (Continuous Integration Continious Delivery - CI&CD), prettier&eslint (code formatting), husky (pre-commit and pre-push script), npm (package manager)


## 30/01: Training - Basic project preparation
- Some differences in skills in the group lead to different tasks for each member of the group. Aurélie and Romain start the training of the technologies used and Lilian initiates the base and the architecture of the project.

## 06/02: Training - Beginning of tooling
- Aurélie is absent for an urgent reason (world cup).
- Aurélie and Romain continue their training on Front technologies.
- Lilian starts tooling the code with jest and vitest technologies.

## 13/02: Pedagogical break
- Fixing bugs in the repos in order to be independent of machines that would clone those.

## 20/02: Review of the code base - Preparation of the workflow
- Presentation of the code base initiated by Lilian and study of the project architecture.
- Preparation of a workflow diagram to visualize the different steps of the user experience.

## 27-28/02: Slide preparation - Additional training - Weekly task list
- Preparation of presentation's slides and summary of the project progress.
- Deepening of some Front technologies (Vue Router, Pinia...).
- Establishment of a weekly task list for the progress of the project and assignment of tasks.

## 06-07/03 : Slider Component(front) - Login Page(back ~, front) - Begining of all view basicly(front)
- Romain : Slider (front) 
  - Done : 
    - Slider component
    - Slider view
    - Add slider to the routerw
  - TODO : 
    - Implement slider tests
    - Implement slider data in the database
- Lilian : Authentification (back ~, front)
  - DONE : 
    - Login and signup api
    - Login tests
    - Login and signup documentation
  - TODO : 
    - Implement signup tests
    - Implement update role or email or password
- Aurélie : Begining of all view basicly(front)
  - DONE : 
    - Navbar component
  - TODO : 
    - Implement all basic components
    - Implement all basic views

## 13-14/03 : Begining of experiment (back) - AdminPanel (back) - All view (front)
- Romain : Begining of experiment (back)
  - DONE : 
    - Experiment model
    - Implement experiment api
    - Implement experiment data in the database
  - TODO : 
    - Experiment documentation
    - Experiment set of experiments
- Lilian : AdminPanel (back)
  - DONE : 
    - update role api
    - update role tests
    - update role documentation
    - other feature just for control token from the front
  - TODO :
    - Stats data, view or/and think about sessions
- Aurelie : All view (front)
  - DONE : 
    - Implement all basic components
    - Implement all basic views
  - TODO : 
    - Implement experience and set of experience (store, composables) with real data

## 20-21/03 :  - Track progress of users (back/front) - Help Button (front)
- Romain : ...
  - DONE : 
    - Connect the SliderText to the API to get instructions
    - Test of the API for experiences
  - TODO : 
    - Upgrade Experience Model
    - Adapt fetch methods using AXIOS
    - Move the getApi method to composable
    - Realize documentations
    - Implement session (back)
- Lilian : Track progress of users (back/front)
  - DONE : 
    - Implement progress data
    - Implement progress api (create, patch and get)
    - Implement progress tests
    - Implement front progress store
    - Implement front progress composables
    - Implement front progress components
    - Rewrite Readme to document the project and the way to use it
  - TODO : 
    - Implement progress documentation
    - Implement session (front)
    - Modify user to deal with user with no email and no password
- Aurelie : Help button / store (front)
  - DONE : 
    - Move help button on navbar
  - TODO : 
    - implement experiment view using store
    - put the experiments texts given by MAT
    - Upgrade Home view

## 27-28/03 : rien
- Romain : ...
  - DONE : 
    - ...
- Lilian : Modify user to deal with user with no email and no password 
  - DONE : 
    - Modify user to deal with user with no email and no password
    - Merge all of my features branch
- Aurélie : ...
  - DONE : 
    - ...
